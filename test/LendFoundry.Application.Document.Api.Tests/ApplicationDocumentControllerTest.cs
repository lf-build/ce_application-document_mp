﻿using LendFoundry.Foundation.Date;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.DocumentManager;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Application.Document.Api.Controllers;
using Xunit;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using System.IO;
using LendFoundry.Foundation.Services;

using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Mime;
using LendFoundry.Application.Document.Mocks;
using LendFoundry.EventHub.Client;

namespace LendFoundry.Application.Document.Api.Tests
{
    public class ApplicationDocumentControllerTest
    {
        [Fact]
        public void AddApplicationDucumentReturnOk()
        {
            IFormFile file = GetFileObject();

            Dictionary<string, string> lookUpData = new Dictionary<string, string>() { { "1", "sss" } };

            //Lookup service Mocking
            var lookupService = new Mock<ILookupService>();
            lookupService.Setup(m => m.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(lookUpData);


            IDocument dataManagerDocument = new LendFoundry.DocumentManager.Document { Id = "1", FileName = "Test" };
            var documentManagerService = new Mock<IDocumentManagerService>();
            documentManagerService.Setup(m => m.Create(new MemoryStream(new byte[28]), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), null)).ReturnsAsync(dataManagerDocument);

            var response = GetController(lookupService.Object, documentManagerService.Object).CreateDocument("1", "1", "1", file, "test");
            Assert.ThrowsAsync<DocumentException>(() => { throw new DocumentException("The Document cannot be uploaded"); }); // temp

            //Assert.IsType<HttpOkObjectResult>(response.Result);
            //var application = response as IApplicationDocument;
            //Assert.Equal("1", application.Category);
            //Assert.Equal("1", application.StipulationType);
        }



        [Fact]
        public void AddApplicationDucumentReturn400()
        {
            IFormFile file = GetFileObject();            
            var response = GetController().CreateDocument(null, "1", "1", file,"tst");
            Assert.IsType<ErrorResult>(response.Result);
            var result = ((IActionResult)response.Result) as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }
      

       

      

       


        //[Fact]
        //public async void GetByApplicationIdReturnOk()
        //{
        //    List<IApplicationDocument> dummyApplicationDocuments = GetDummyDocumentData();


        //    var response = await GetController(dummyApplicationDocuments).GetByApplicationId("1");
        //    Assert.IsType<HttpOkObjectResult>(response);
        //    var application = ((HttpOkObjectResult)response).Value as List<IApplicationDocument>;
        //    Assert.NotEmpty(application);
        //    Assert.Equal(application[0].ApplicationId, dummyApplicationDocuments[0].ApplicationId);

        //}

        //[Fact]
        //public void GetByApplicationIdReturn404()
        //{

        //    List<IApplicationDocument> dummyApplicationDocuments = GetDummyDocumentData();

        //    var applicationDocumentController = GetController(dummyApplicationDocuments);
        //    var response = applicationDocumentController.GetByApplicationNumber("456");
        //    Assert.IsType<ErrorResult>(response.Result);
        //    var result = ((IActionResult)response.Result) as ErrorResult;
        //    Assert.NotNull(result);
        //    Assert.Equal(result.StatusCode, 404);
        //}

        [Fact]
        public void GetApplicationReturn400()
        {
            var applicationDocumentController = GetController();
            var response = applicationDocumentController.GetByApplicationNumber(null);
            Assert.IsType<ErrorResult>(response.Result);
            var result = ((IActionResult)response.Result) as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }


       

      

    

       
       

        private static ApplicationDocumentController GetController(IEnumerable<IApplicationDocument> applicationDocumnets = null)
        {
            //var applicantService = new FakeApplicationDocumentService(new UtcTenantTime(), applicationDocumnets ?? new List<IApplicationDocument>());

            return new ApplicationDocumentController(GetApplicationDocumentService(applicationDocumnets));
        }
        private static FakeApplicationDocumentRepository GetApplicationDocumentRepository(IEnumerable<IApplicationDocument> applicationDocuments = null)
        {
            return new FakeApplicationDocumentRepository(new UtcTenantTime(), applicationDocuments ?? new List<IApplicationDocument>());

        }
        private static ApplicationDocumentController GetController(ILookupService lookup, IDocumentManagerService documentManagerService ,IEnumerable<IApplicationDocument> applicationDocumnets = null)
        {
            //var applicantService = new FakeApplicationDocumentService(new UtcTenantTime(), applicationDocumnets ?? new List<IApplicationDocument>());

            return new ApplicationDocumentController(GetApplicationDocumentService(lookup, documentManagerService,applicationDocumnets));
        }

        private static IApplicationDocumentService GetApplicationDocumentService(IEnumerable<IApplicationDocument> applicationDocuments = null)
        {
            return new ApplicationDocumentService(new FakeApplicationDocumentRepository(new UtcTenantTime(), applicationDocuments ?? new List<IApplicationDocument>()), Mock.Of<IDocumentManagerService>(), Mock.Of<ILookupService>(),null, Mock.Of<IEventHubClient>());
        }

        private static IApplicationDocumentService GetApplicationDocumentService( ILookupService lookup , IDocumentManagerService documentManagerService,IEnumerable<IApplicationDocument> applicationDocuments = null)
        {
            return new ApplicationDocumentService(new FakeApplicationDocumentRepository(new UtcTenantTime(), applicationDocuments ?? new List<IApplicationDocument>()), documentManagerService, lookup,null, Mock.Of<IEventHubClient>());
        }

        private static IFormFile GetFileObject()
        {

            //Mocking for IFormFile
            var fileMock = new Mock<IFormFile>();
            var s = "Hello World from a Fake File";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(s);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(m => m.OpenReadStream()).Returns(ms);
            fileMock.Setup(m => m.ContentDisposition).Returns("'form-data; name='file'; filename='fiddler.txt'");

            var file = fileMock.Object;
            return file;
        }

        private static List<IApplicationDocument> GetDummyDocumentData()
        {
            return new List<IApplicationDocument>
            {
                new ApplicationDocument
                {
                    ApplicantId = "1",
                    ApplicationId = "1",
                    Category = "DriverLicense",                 
                    Id = "1"
                },
                 new ApplicationDocument
                {
                    ApplicantId = "1",
                    ApplicationId = "2",
                    Category = "DriverLicense",                   
                    Id = "2"
                }
            };
        }
    }
}
