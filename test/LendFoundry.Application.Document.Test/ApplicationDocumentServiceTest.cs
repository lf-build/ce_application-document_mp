﻿using LendFoundry.Application.Document.Mocks;
using LendFoundry.DocumentManager;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;

namespace LendFoundry.Application.Document.Test
{
    public class ApplicationDocumentServiceTest
    {
        #region PrivateMethods

        private static IApplicationDocumentService GetDocumentApplicationService()
        {
            return new ApplicationDocumentService(Mock.Of<IApplicationDocumentRepository>(), Mock.Of<IDocumentManagerService>(), Mock.Of<ILookupService>(),null, Mock.Of<IEventHubClient>());
        }

        private static IApplicationDocumentService GetApplicationFakeDocumentService(IEnumerable<IApplicationDocument> applicationDocuments = null)
        {
            return new ApplicationDocumentService(new FakeApplicationDocumentRepository(new UtcTenantTime(), applicationDocuments ?? new List<IApplicationDocument>()), Mock.Of<IDocumentManagerService>(), Mock.Of<ILookupService>(),null, Mock.Of<IEventHubClient>());
        }

        private static IApplicationDocumentService GetApplicationDocumentObjectFake(IDocumentManagerService documentManager, ILookupService lookupservice, IEnumerable<IApplicationDocument> applicationDocuments = null)
        {
            return new ApplicationDocumentService(new FakeApplicationDocumentRepository(new UtcTenantTime(), applicationDocuments ?? new List<IApplicationDocument>()), documentManager, lookupservice,null, Mock.Of<IEventHubClient>());
                
        }


        private static Dictionary<string, string> LookUpData()
        {
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("DriverLicense", "1");
            return categoryLookup;
        }

        private static FakeApplicationDocumentRepository GetApplicationDocumentRepository(IEnumerable<IApplicationDocument> applicationDocuments = null)
        {
            return new FakeApplicationDocumentRepository(new UtcTenantTime(), applicationDocuments ?? new List<IApplicationDocument>());

        }


        private static IDocument GetDummyDocumentDataWithNullId()
        {
            IDocument documentData = new LendFoundry.DocumentManager.Document();
            documentData.Id = null;
            documentData.FileName = "Test";
            return documentData;
        }

        private static IDocument GetDummyDocumentDataWithValidOutPut()
        {
            IDocument documentData = new LendFoundry.DocumentManager.Document();
            documentData.Id = null;
            documentData.FileName = "Test";
            return documentData;
        }

        private static byte[] GetBytes()
        {
            string input = "some text";
            byte[] array = Encoding.ASCII.GetBytes(input);
            return array;
        }

        private static List<IApplicationDocument> DummyApplicationDocumnetData()
        {
            return new List<IApplicationDocument>
            {
                new ApplicationDocument
                {
                    ApplicantId = "1",
                    ApplicationId = "1",
                    Category = "DriverLicense",                
                     Id = "Test1"
                },
                 new ApplicationDocument
                {
                    ApplicantId = "1",
                    ApplicationId = "2",
                    Category = "DriverLicense",                 
                     Id = "Test2"
                }
            };
        }
        #endregion

      

        

        #region Application
        [Fact]
        public void Should_Throw_Exception_When_Application_Id_Null()
        {
            var applicationDocumentService = GetDocumentApplicationService();

            applicationDocumentService.GetByApplicationNumber(null);

            Assert.ThrowsAsync<ArgumentException>(() => { throw new ArgumentException("Argument is null or whitespace"); });

        }

        //[Fact]
        //public async void Should_Get_ApplicationDocument_When_ApplicationId_IsValid()
        //{
        //    List<IApplicationDocument> dummyApplicationDocuments = DummyApplicationDocumnetData();
        //    var applicationDocumentService = GetApplicationFakeDocumentService(dummyApplicationDocuments);

        //    var applicationDocuments = await applicationDocumentService.GetByApplicationId("1");

        //   // Assert.NotNull(applicationDocuments.Where(i => i.ApplicantId == "1").FirstOrDefault());

        //}

        [Fact]
        public void Should_Get_Exception_When_ApplicationId_Is_NotValid()
        {
            List<IApplicationDocument> dummyApplicationDocuments = DummyApplicationDocumnetData();
            var applicationDocumentService = GetApplicationFakeDocumentService(dummyApplicationDocuments);
            applicationDocumentService.GetByApplicationNumber("3");

            Assert.ThrowsAsync<NotFoundException>(() => { throw new NotFoundException("Document for 3 not found"); });

        }
        #endregion


        #region AddDocument

        [Fact]
        public void AddDocument_Should_Throw_Exception_When_ApplicationId_Null()
        {
            var applicationDocumentService = GetDocumentApplicationService();
            byte[] array = GetBytes();

            applicationDocumentService.Add(null, "1", "2", array, "Test.txt", new List<string>());

            Assert.ThrowsAsync<ArgumentException>(() => { throw new ArgumentException("Argument is null or whitespace"); });

        }

        [Fact]
        public void AddDocument_Should_Throw_Exception_When_Category_Null()
        {
            var applicationDocumentService = GetDocumentApplicationService();
            byte[] array = GetBytes();

            applicationDocumentService.Add("1", null, "2", array, "Test.txt", new List<string>());

            Assert.ThrowsAsync<ArgumentException>(() => { throw new ArgumentException("Argument is null or whitespace"); });

        }

        [Fact]
        public void AddDocument_Should_Throw_Exception_When_StipulationType_Null()
        {
            var applicationDocumentService = GetDocumentApplicationService();
            byte[] array = GetBytes();

            applicationDocumentService.Add("1", "1", null, array, "Test.txt",new List<string>());

            Assert.ThrowsAsync<ArgumentException>(() => { throw new ArgumentException("Argument is null or whitespace"); });

        }

        [Fact]
        public void AddDocument_Should_Throw_Exception_When_Category_NotValid()
        {
            var applicationDocumentService = GetDocumentApplicationService();
            byte[] array = GetBytes();
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("DriverLicense", "1");          


            Mock<ILookupService> _mockLookupService = new Mock<ILookupService>();
            _mockLookupService.Setup(x => x.GetLookupEntry("applicationDocument-category", "1")).Returns(categoryLookup);

            applicationDocumentService.Add("1", "2", "2", array, "Test.txt",new List<string>());

            Assert.ThrowsAsync<InvalidArgumentException>(() => { throw new InvalidArgumentException("The category  doesn't exist in the list of categories."); });

        }

      

    

        [Fact]
        public  void AddDocument_Should_Throw_Exception_When_Document_Is_Not_Uploded()
        {

            byte[] array = GetBytes();

            Dictionary<string, string> categoryLookup = LookUpData();

          
            List<IApplicationDocument> dummyApplicationDocuments = DummyApplicationDocumnetData();
            Mock<IDocumentManagerService> _mockDocumentManagerService = new Mock<IDocumentManagerService>();
            _mockDocumentManagerService.Setup(x => x.Create(new MemoryStream(array), "application", dummyApplicationDocuments.FirstOrDefault().ApplicationId, "Test", null)).ReturnsAsync(null);


            Mock<ILookupService> _mockLookupService = new Mock<ILookupService>();

            _mockLookupService.SetupSequence(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()))
                    .Returns(categoryLookup)
                    .Returns(categoryLookup);


            var applicationDocumentService = GetApplicationDocumentObjectFake(_mockDocumentManagerService.Object, _mockLookupService.Object, dummyApplicationDocuments);

             applicationDocumentService.Add(dummyApplicationDocuments.FirstOrDefault().ApplicationId, dummyApplicationDocuments.FirstOrDefault().Category,string.Empty, array, "Test.txt", new List<string>());

             Assert.ThrowsAsync<DocumentException>(() => { throw new DocumentException("The Document cannot be uploaded"); });

        }


        [Fact]
        public  void AddDocument_Should_Throw_Exception_When_Document_Is_Uploded_With_Null_DocumentId()
        {

            byte[] array = GetBytes();

            Dictionary<string, string> categoryLookup = LookUpData();

            IDocument documentData = GetDummyDocumentDataWithNullId();

            List<IApplicationDocument> dummyApplicationDocuments = DummyApplicationDocumnetData();

            Mock<IDocumentManagerService> _mockDocumentManagerService = new Mock<IDocumentManagerService>();
            _mockDocumentManagerService.Setup(x => x.Create(new MemoryStream(array), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), null)).ReturnsAsync(documentData);


            Mock<ILookupService> _mockLookupService = new Mock<ILookupService>();

            _mockLookupService.SetupSequence(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()))
                    .Returns(categoryLookup)
                    .Returns(categoryLookup);


            var applicationDocumentService = GetApplicationDocumentObjectFake(_mockDocumentManagerService.Object, _mockLookupService.Object, dummyApplicationDocuments);

             applicationDocumentService.Add(dummyApplicationDocuments.FirstOrDefault().ApplicationId, dummyApplicationDocuments.FirstOrDefault().Category, string.Empty, array, "Test.txt", new List<string>());

             Assert.ThrowsAsync<DocumentException>(() => { throw new DocumentException("Test is not found"); });

        }


        [Fact]
        public  void AddDocument_Should_Add_ApplicationDocument_When_All_Input_Valid()
        {

            byte[] array = GetBytes();

            Dictionary<string, string> categoryLookup = LookUpData();

            IDocument documentData = GetDummyDocumentDataWithValidOutPut();
            List<IApplicationDocument> dummyApplicationDocuments = DummyApplicationDocumnetData();
            Mock<IDocumentManagerService> _mockDocumentManagerService = new Mock<IDocumentManagerService>();
            _mockDocumentManagerService.Setup(x => x.Create(It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), null)).ReturnsAsync(documentData);


            Mock<ILookupService> _mockLookupService = new Mock<ILookupService>();

            _mockLookupService.SetupSequence(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>()))
                    .Returns(categoryLookup)
                    .Returns(categoryLookup);


            var applicationDocumentService = GetApplicationDocumentObjectFake(_mockDocumentManagerService.Object, _mockLookupService.Object, dummyApplicationDocuments);

             applicationDocumentService.Add(dummyApplicationDocuments.FirstOrDefault().ApplicationId, dummyApplicationDocuments.FirstOrDefault().Category, string.Empty, array, "Test.txt", new List<string>());

             Assert.Equal(dummyApplicationDocuments.FirstOrDefault().ApplicationId, "1");

        }


        #endregion

    }
}
