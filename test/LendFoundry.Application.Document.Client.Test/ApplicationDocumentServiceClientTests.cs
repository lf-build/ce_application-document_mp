﻿using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Application.Document.Client.Test
{
    public class ApplicationDocumentServiceClientTests
    {
        private ApplicantDocumentService ApplicationDocumentServiceClient { get; }
        private IRestRequest Request { get; set; }

        private Mock<IServiceClient> MockServiceClient { get; }

        private static List<IApplicationDocument> DummyApplicationDocumnetData()
        {
            return new List<IApplicationDocument>
            {
                new ApplicationDocument
                {
                    ApplicantId = "1",
                    ApplicationId = "1",
                    Category = "DriverLicense",             
                     Id = "Test1"
                },
                 new ApplicationDocument
                {
                    ApplicantId = "1",
                    ApplicationId = "2",
                    Category = "DriverLicense",                   
                     Id = "Test2"
                }
            };
        }

        private static byte[] GetBytes()
        {
            string input = "some text";
            byte[] array = Encoding.ASCII.GetBytes(input);
            return array;
        }

        public ApplicationDocumentServiceClientTests()
        {
            //Request = new RestRequest();
            MockServiceClient = new Mock<IServiceClient>();
            ApplicationDocumentServiceClient = new ApplicantDocumentService(MockServiceClient.Object);
        }

        //[Fact]
        //public async void Client_GetByApplicationId()
        //{
        //    List<ApplicationDocument> test = GetDummyData();
        //    MockServiceClient.Setup(s => s.ExecuteAsync<List<ApplicationDocument>>(It.IsAny<IRestRequest>()))
        //       .ReturnsAsync(test)
        //        .Callback<IRestRequest>(r => Request = r);



        //    var result = await ApplicationDocumentServiceClient.GetByApplicationId("1");
        //    Assert.Equal("/application/{applicationId}", Request.Resource);
        //    Assert.Equal(Method.GET, Request.Method);
        //    Assert.NotNull(result);
        //}

        private static List<ApplicationDocument> GetDummyData()
        {
            return new List<ApplicationDocument>
            {
                new ApplicationDocument
                {
                    ApplicantId = "1",
                    ApplicationId = "1",
                    Category = "DriverLicense",                 
                     Id = "Test1"
                },
                 new ApplicationDocument
                {
                    ApplicantId = "1",
                    ApplicationId = "2",
                    Category = "DriverLicense",                   
                     Id = "Test2"
                }
            };
        }

        


        

        //[Fact]
        //public async void Client_Add_With_Application()
        //{
        //    List<ApplicationDocument> dummydata = GetDummyData();
        //    byte[] array = GetBytes();
        //    MockServiceClient.Setup(s => s.ExecuteAsync<ApplicationDocument>(It.IsAny<IRestRequest>()))
        //        .Callback<IRestRequest>(r => Request = r)
        //        .ReturnsAsync(dummydata.FirstOrDefault());
                    

        //    var result = await ApplicationDocumentServiceClient.Add("1","1","1", array,"Test", new List<string>());
        //    Assert.Equal("{applicationId}/{category}/{stipulationType}", Request.Resource);
        //    Assert.Equal(Method.POST, Request.Method);
        //}

      

      
    }
}
