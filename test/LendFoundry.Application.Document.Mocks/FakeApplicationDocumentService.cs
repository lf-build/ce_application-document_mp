﻿using LendFoundry.DocumentManager;
using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Application.Document.Mocks
{
    public class FakeApplicationDocumentService : IApplicationDocumentService
    {

        public ITenantTime TenantTime { get; set; }
        public List<IApplicationDocument> ApplicationDocuments { get; } = new List<IApplicationDocument>();
        public int CurrentId { get; set; }

        private IDocumentManagerService DocumentManager { get; set; }

        public FakeApplicationDocumentService(ITenantTime tenantTime, IEnumerable<IApplicationDocument> applicationDocuments) : this(tenantTime)
        {
            ApplicationDocuments.AddRange(applicationDocuments);
            CurrentId = ApplicationDocuments.Count() + 1;
        }

        public FakeApplicationDocumentService(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
            CurrentId = 1;
        }

       
        public Task<IApplicationDocument> Add(string applicationId, string category, byte[] fileBytes, string fileName,List<string> tags)
        {
            // var uploadedDocument =  DocumentManager.Create(new MemoryStream(fileBytes), "application", applicationId, fileName, null); // TODO : What is Tag ?

            IApplicationDocument applicationdocument = new ApplicationDocument
            {
                ApplicationId = applicationId,
                ApplicantId = applicationId,
                Category = category,
                DocumentId = "uplodedDocumentId"           
            };
            ApplicationDocuments.Add(applicationdocument);
            return Task.FromResult(applicationdocument);
        }

        public Task<IApplicationDocument> Add(string applicationId, string applicantId, string category, byte[] fileBytes, string fileName, List<string> tags)
        {
            IApplicationDocument applicationdocument = new ApplicationDocument
            {
                ApplicationId = applicationId,
                ApplicantId = applicationId,
                Category = category,
                DocumentId = "uplodedDocumentId"           
            };
            ApplicationDocuments.Add(applicationdocument);
            return Task.FromResult(applicationdocument);
        }
        public Task<IEnumerable<IDocumentWithApplication>> GetByApplicationNumber(string applicationId)
        {
            return null;
                //Task.FromResult(ApplicationDocuments.Where(a=> a.ApplicationId.Equals(applicationId)).ToList());
        }
        public Task<List<LendFoundry.DocumentManager.IDocument>> GetByCategory(string applicationId,string category)
        {
            return null;
            //Task.FromResult(ApplicationDocuments.Where(a=> a.ApplicationId.Equals(applicationId)).ToList());
        }

        public Task<byte[]> DownloadDocumentsByApplicationNumber(string applicationNumber)
        {
            return null;
        }

        public Task<IApplicationDocument> Add(string applicationNumber, string category, byte[] fileBytes, string fileName, List<string> tags, object metaData = null)
        {
            throw new NotImplementedException();
        }

        public Task<IApplicationDocument> ChangeCategory(string applicationNumber, string documentId, string category)
        {
            throw new NotImplementedException();
        }

        public Task<IApplicationDocument> VerifyDocument(string applicationNumber, string documentId, string status)
        {
            throw new NotImplementedException();
        }

        public Task<IDownloadResponseModel> DownloadDocumentByDocumentId(string entityType, string entityId, string documentId)
        {
            throw new NotImplementedException();
        }

        public Task<byte[]> DownloadDocumentsByCategory(string applicationNumber, string[] category)
        {
            throw new NotImplementedException();
        }

        public Task<IApplicationDocument> Add(string entityType, string applicationNumber, string category, byte[] fileBytes, string fileName, List<string> tags, object metaData = null)
        {
            throw new NotImplementedException();
        }

        public Task<IApplicationDocument> Add(string entityType, string applicationNumber, string applicantId, string category, byte[] fileBytes, string fileName, List<string> tags)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IDocumentWithApplication>> GetByApplicationNumber(string entityType, string applicationNumber)
        {
            throw new NotImplementedException();
        }

        public Task<List<IDocument>> GetByCategory(string entityType, string applicationNumber, string category)
        {
            throw new NotImplementedException();
        }

        public Task<byte[]> DownloadDocumentsByApplicationNumber(string entityType, string applicationNumber)
        {
            throw new NotImplementedException();
        }

        public Task<IApplicationDocument> ChangeCategory(string entityType, string applicationNumber, string documentId, string category)
        {
            throw new NotImplementedException();
        }

        public Task<IApplicationDocument> VerifyDocument(string entityType, string applicationNumber, string documentId, string status)
        {
            throw new NotImplementedException();
        }

        public Task<byte[]> DownloadDocumentsByCategory(string entityType, string applicationNumber, string[] category)
        {
            throw new NotImplementedException();
        }

        public Task<byte[]> DownloadVerifiedDocuments(string applicationNumber, string[] category)
        {
            throw new NotImplementedException();
        }

        public Task<byte[]> DownloadVerifiedDocuments(string entityType, string applicationNumber, string[] category)
        {
            throw new NotImplementedException();
        }
    }
}
