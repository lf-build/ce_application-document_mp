﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Services;

namespace LendFoundry.Application.Document.Mocks
{
    public class FakeApplicationDocumentRepository : IApplicationDocumentRepository
    {
        public ITenantTime TenantTime { get; set; }
        public List<IApplicationDocument> ApplicationDocuments { get; } = new List<IApplicationDocument>();

        public FakeApplicationDocumentRepository(ITenantTime tenantTime, IEnumerable<IApplicationDocument> applicationDocuments) : this(tenantTime)
        {
            ApplicationDocuments.AddRange(applicationDocuments);
        }
        public FakeApplicationDocumentRepository(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        public Task<List<IApplicationDocument>> GetByApplicantId(string applicantId)
        {
            var result = Task.FromResult(ApplicationDocuments.Where(applicant => applicant.ApplicantId.Equals(applicantId)).ToList());
            return result;

        }

        public Task<List<IApplicationDocument>> Get(string entityType, string entityId, string applicantId)
        {
            var result = Task.FromResult(ApplicationDocuments.Where(a => a.ApplicantId.Equals(applicantId) && a.ApplicationId.Equals(entityId)).ToList());
            return result;

        }
        //public Task<List<IApplicationDocument>> Get(string applicationDocumentId)
        //{
        //    var result = Task.FromResult(ApplicationDocuments.Where(a => a.Id.Equals(applicationDocumentId) ).ToList());
        //    return result.Result.Count == 0 ? null : result;

        //}

        public Task<List<IApplicationDocument>> GetByApplicationId(string entityType, string entityId)
        {
            var result = Task.FromResult(ApplicationDocuments.Where(a => a.ApplicationId.Equals(entityId)).ToList());
            return result;
        }



        public Task<IApplicationDocument> Get(string id)
        {

            var result = Task.FromResult(ApplicationDocuments.Where(a => a.Id.Equals(id)).FirstOrDefault());
            return result;

        }

        public Task<IEnumerable<IApplicationDocument>> All(Expression<Func<IApplicationDocument, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            throw new NotImplementedException();
        }

        public void Add(IApplicationDocument item)
        {
            ApplicationDocuments.Add(item);

        }

        public void Remove(IApplicationDocument item)
        {
            throw new NotImplementedException();
        }

        public void Update(IApplicationDocument item)
        {
            throw new NotImplementedException();
        }

        public int Count(Expression<Func<IApplicationDocument, bool>> query)
        {
            throw new NotImplementedException();
        }

        Task<List<IApplicationDocument>> IApplicationDocumentRepository.GetByApplicantId(string ApplicantId)
        {
            throw new NotImplementedException();
        }

        Task<List<IApplicationDocument>> IApplicationDocumentRepository.GetByCategory(string entityType, string entityId, string category)
        {
            throw new NotImplementedException();
        }

      
        Task<IApplicationDocument> IRepository<IApplicationDocument>.Get(string id)
        {
            throw new NotImplementedException();
        }

        Task<IEnumerable<IApplicationDocument>> IRepository<IApplicationDocument>.All(Expression<Func<IApplicationDocument, bool>> query, int? skip, int? quantity)
        {
            throw new NotImplementedException();
        }

        void IRepository<IApplicationDocument>.Add(IApplicationDocument item)
        {
            throw new NotImplementedException();
        }

        void IRepository<IApplicationDocument>.Remove(IApplicationDocument item)
        {
            throw new NotImplementedException();
        }

        void IRepository<IApplicationDocument>.Update(IApplicationDocument item)
        {
            throw new NotImplementedException();
        }

        int IRepository<IApplicationDocument>.Count(Expression<Func<IApplicationDocument, bool>> query)
        {
            throw new NotImplementedException();
        }

        public Task<IApplicationDocument> GetByDocumentId(string documentId)
        {
            throw new NotImplementedException();
        }

        public Task<List<IApplicationDocument>> GetByCategory(string entityType, string applicationId, string[] category)
        {
            throw new NotImplementedException();
        }
    }
}