﻿using System;

namespace LendFoundry.Application.Document
{
    public class ApproveDocumentRequest : IApproveDocumentRequest
    {
        public string DocumentId { get; set; }
        public DateTimeOffset ApprovedDate { get; set; }
        public string ApprovedBy { get; set; }
    }
}
