﻿using System.Collections.Generic;

namespace LendFoundry.Application.Document
{
    public class ApplicantDocumentUploadRequest : IApplicantDocumentUploadRequest
    {
        public string DocumentId { get; set; }
        public List<IUploadedDocument> UploadedDocuments { get; set; }
    }
}
