﻿using System;

namespace LendFoundry.Application.Document
{
    public interface IRejectDocumentRequest
    {
       
        string RejectedReason { get; set; }
    }
}
