﻿using System;

namespace LendFoundry.Application.Document
{
    public class RejectDocumentRequest : IRejectDocumentRequest
    {
     
        public string RejectedReason { get; set; }
    }
}
