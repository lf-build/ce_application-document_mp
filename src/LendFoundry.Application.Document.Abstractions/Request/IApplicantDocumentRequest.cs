﻿#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif

using System;
using System.Collections.Generic;

namespace LendFoundry.Application.Document
{
    public interface IApplicantDocumentRequest
    {
        IFormFile fileData { get; set; }
        //string Category { get; set; }
        //DateTimeOffset RequestDate { get; set; }
        //string Type { get; set; }
    }
}
