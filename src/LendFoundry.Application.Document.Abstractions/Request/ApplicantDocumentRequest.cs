﻿using LendFoundry.Foundation.Client;
#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif

using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.Application.Document
{
    public class ApplicantDocumentRequest : IApplicantDocumentRequest
    {
        [JsonConverter(typeof(InterfaceConverter<IApplicantDocumentRequest, ApplicantDocumentRequest>))]
        public IFormFile fileData { get; set; }
        //public string Category { get; set; }
        //public DateTimeOffset RequestDate { get; set; }
        //public string Type { get; set; }
    }
}
