﻿using System;

namespace LendFoundry.Application.Document
{
    public interface IApproveDocumentRequest
    {
        string DocumentId { get; set; }
        DateTimeOffset ApprovedDate { get; set; }
        string ApprovedBy { get; set; }
    }
}
