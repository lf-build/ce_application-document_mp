﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Application.Document
{
    public interface IApplicantDocumentUploadRequest
    {
        string DocumentId { get; set; }
        List<IUploadedDocument> UploadedDocuments { get; set; }
    }
}
