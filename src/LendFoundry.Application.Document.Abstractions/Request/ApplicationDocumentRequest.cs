﻿#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif

using System;

namespace LendFoundry.Application.Document
{
    public class ApplicationDocumentRequest : IApplicationDocumentRequest
    {
       
        public IFormFile fileData { get; set; }
    }
}