﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Application.Document
{
    public interface IApplicationDocument : IAggregate
    {
        string EntityType { get; set; }
        string ApplicantId { get; set; }
        string ApplicationId { get; set; }
        string Category { get; set; }
        string DocumentId { get; set; }
        string GroupName { get; set; }
        DocumentVerificationStatus DocumentStatus { get; set; }
        DateTimeOffset Timestamp { get; set; }
        string DocumentFcuStatus { get; set; }
    }
}
