﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Application.Document
{
    public interface IDocumentWithApplication
    {
        string EntityType { get; set; }
        string ApplicantId { get; set; }
        string ApplicationId { get; set; }
        string Category { get; set; }
        string FileName { get; set; }
        string Id { get; set; }
        object Metadata { get; set; }
        long Size { get; set; }
        List<string> Tags { get; set; }
        DateTimeOffset Timestamp { get; set; }
        DocumentVerificationStatus DocumentStatus { get; set; }
        string DocumentId { get; set; }
        string DocumentFcuStatus { get; set; }
    }
}
