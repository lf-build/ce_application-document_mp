﻿using System;

namespace LendFoundry.Application.Document
{
    public interface IUploadedDocument
    {
        string DocumentURL { get; set; }

        DateTimeOffset UploadedDate { get; set; }

        string UploadedBy { get; set; }
    }
}
