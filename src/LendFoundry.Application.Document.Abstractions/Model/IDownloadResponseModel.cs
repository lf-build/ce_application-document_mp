﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Application.Document
{
    public interface IDownloadResponseModel
    {
        string DownloadString { get; set; }
    }
}
