﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Application.Document
{
    public class DownloadResponseModel :IDownloadResponseModel
    {
        public string DownloadString { get; set; }
    }
}
