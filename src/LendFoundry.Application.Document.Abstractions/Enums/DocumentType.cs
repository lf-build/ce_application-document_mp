﻿namespace LendFoundry.Application.Document
{
    public enum DocumentType
    {
        DriverLicense = 1,
        ElectricityBill = 2,
        LandlineBill = 3,
        MobileBill = 4,
        InternetBill = 5,
        CableBill = 6,
        UIDDocument = 7,
        PayStub = 8,
        Photo = 9,
        Other = 10
    }
}
