﻿namespace LendFoundry.Application.Document
{
    public enum DocumentVerificationStatus
    {
        Verifiable,
        Unverifiable
    }
}
