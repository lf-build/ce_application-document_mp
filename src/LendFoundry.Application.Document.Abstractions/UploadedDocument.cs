﻿using System;

namespace LendFoundry.Application.Document
{
    public class UploadedDocument : IUploadedDocument
    {
        public string DocumentURL { get; set; }
        public DateTimeOffset UploadedDate { get; set; }
        public string UploadedBy { get; set; }
    }
}
