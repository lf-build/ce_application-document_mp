﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Application.Document
{
    [Serializable]
    public class DocumentException : Exception
    {
        public DocumentException()
        {

        }
        public DocumentException(string message) : base(message)
        {

        }

       
    }
}
