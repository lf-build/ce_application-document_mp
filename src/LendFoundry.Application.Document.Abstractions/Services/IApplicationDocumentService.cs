﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.DocumentManager;


namespace LendFoundry.Application.Document
{
    public interface IApplicationDocumentService
    {
        Task<IApplicationDocument> Add(string applicationNumber, string category, byte[] fileBytes, string fileName, List<string> tags, object metaData = null);
        Task<IApplicationDocument> Add(string applicationNumber, string applicantId, string category, byte[] fileBytes, string fileName, List<string> tags);
        Task<IApplicationDocument> Add(string entityType, string applicationNumber, string category, byte[] fileBytes, string fileName, List<string> tags, object metaData = null);
        Task<IApplicationDocument> Add(string entityType, string applicationNumber, string applicantId, string category, byte[] fileBytes, string fileName, List<string> tags);
        Task<IEnumerable<IDocumentWithApplication>> GetByApplicationNumber(string applicationNumber);
        Task<IEnumerable<IDocumentWithApplication>> GetByApplicationNumber(string entityType,string applicationNumber);
        Task<List<IDocument>> GetByCategory(string applicationNumber, string category);
        Task<List<IDocument>> GetByCategory(string entityType,string applicationNumber, string category);

        Task<byte[]> DownloadDocumentsByApplicationNumber(string applicationNumber);
        Task<byte[]> DownloadDocumentsByApplicationNumber(string entityType,string applicationNumber);
        Task<IApplicationDocument> ChangeCategory(string applicationNumber, string documentId, string category);
        Task<IApplicationDocument> ChangeCategory(string entityType,string applicationNumber, string documentId, string category);

        Task<IApplicationDocument> VerifyDocument(string applicationNumber, string documentId, string status);
        Task<IApplicationDocument> VerifyDocument(string entityType,string applicationNumber, string documentId, string status);

        Task<IDownloadResponseModel> DownloadDocumentByDocumentId(string entityType, string entityId, string documentId);
        Task<byte[]> DownloadDocumentsByCategory(string applicationNumber, string[] category);
        Task<byte[]> DownloadDocumentsByCategory(string entityType,string applicationNumber, string[] category);
        Task<byte[]> DownloadVerifiedDocuments(string applicationNumber, string[] category);
        Task<byte[]> DownloadVerifiedDocuments(string entityType,string applicationNumber, string[] category);
        Task<IApplicationDocument> UpdateDocumentFcuStatus(string entityType, string applicationNumber, string documentId, string fcustatus);

    }
}