﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Application.Document
{
    public interface IApplicantDocumentRepositoryFactory
    {
        IApplicantDocumentRepository Create(ITokenReader reader);
    }
}
