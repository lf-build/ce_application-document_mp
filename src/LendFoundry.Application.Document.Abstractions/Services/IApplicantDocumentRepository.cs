﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace LendFoundry.Application.Document
{
    public interface IApplicantDocumentRepository : IRepository<IApplicationDocument>
    {
        #region Applicant - Application Documents      

       
        Task<IList<IApplicationDocument>> GetDocumentsForApplicant(string ApplicantId);
        Task<IList<IApplicationDocument>> GetDocuments(string ApplicationId, string ApplicantId);

        Task<IList<IApplicationDocument>> GetDocumentsForApplication(string applicationId);
        Task<bool> RejectDocument(string documentId, IRejectDocumentRequest request);
        Task<IApplicationDocument> ApproveDocument(IApplicationDocument applicationDocument);

        #endregion
    }
}
