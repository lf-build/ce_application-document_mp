﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace LendFoundry.Application.Document
{
    public interface IApplicationDocumentRepository : IRepository<IApplicationDocument>
    {
        #region Applicant - Application Documents      

       
        Task<List<IApplicationDocument>> GetByApplicantId(string ApplicantId);
        Task<List<IApplicationDocument>> GetByCategory(string entityType, string ApplicationId, string category);

        Task<List<IApplicationDocument>> GetByApplicationId(string entityType, string applicationId);

        Task<IApplicationDocument> GetByDocumentId(string documentId);

        Task<List<IApplicationDocument>> GetByCategory(string entityType, string applicationId, string[] category);
        #endregion
    }
}
