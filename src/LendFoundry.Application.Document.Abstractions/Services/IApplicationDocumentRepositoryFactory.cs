﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Application.Document
{
    public interface IApplicationDocumentRepositoryFactory
    {
        IApplicationDocumentRepository Create(ITokenReader reader);
    }
}
