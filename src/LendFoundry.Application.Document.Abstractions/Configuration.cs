﻿using LendFoundry.Application.Document.Configurations;
using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Application.Document
{
    public class Configuration : IDependencyConfiguration
    {
        public List<ApplicationDocumentConfigurationEntity> Entities { get; set; }

        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
