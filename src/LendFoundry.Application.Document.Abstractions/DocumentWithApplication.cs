﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Application.Document
{
    public class DocumentWithApplication : IDocumentWithApplication
    {
        public string EntityType { get; set; }
        public string ApplicantId { get; set; }
        public string ApplicationId { get; set; }
        public string Category { get; set; }
        public string FileName { get; set; }
        public string Id { get; set; }
        public object Metadata { get; set; }
        public long Size { get; set; }
        public List<string> Tags { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public DocumentVerificationStatus DocumentStatus { get; set; }
        public string DocumentId { get; set; }
        public string DocumentFcuStatus { get; set; }
    }
}
