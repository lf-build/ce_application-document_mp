﻿using System.Collections.Generic;

namespace LendFoundry.Application.Document.Configurations
{
    public class ApplicationDocumentConfiguration 
    {      
        public string GroupName { get; set; }
        public List<string> Category { get; set; }
    }
}
