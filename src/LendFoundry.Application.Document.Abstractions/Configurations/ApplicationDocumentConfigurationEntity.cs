﻿using System.Collections.Generic;

namespace LendFoundry.Application.Document.Configurations
{
    public class ApplicationDocumentConfigurationEntity
    {
        public string EntityName { get; set; }

        public bool IsPreFixApplicationNumber { get; set; }

        public List<ApplicationDocumentConfiguration> ApplicationDocument { get; set; }

    }
}
