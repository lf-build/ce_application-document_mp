﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Application.Document
{
    public class ApplicationDocument : Aggregate, IApplicationDocument
    {
        public string EntityType { get; set; }
        public string ApplicantId { get; set; }
        public string ApplicationId { get; set; }
        public string Category { get; set; }
        public string DocumentId { get; set; }
        public string GroupName { get; set; }
        public DocumentVerificationStatus DocumentStatus { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public string DocumentFcuStatus { get; set; }
    }
}
