﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Application.Document.Events
{
    public class DocumentApproved
    {
        public IApplicationDocument ApplicationDocument { get; set; }
        public DocumentApproved(IApplicationDocument applicationDocument)
        {
            ApplicationDocument = applicationDocument;
        }
    }
}
