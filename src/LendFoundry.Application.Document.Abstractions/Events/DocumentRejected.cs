﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Application.Document.Events
{
    public class DocumentRejected
    {
        public IApplicationDocument ApplicationDocument { get; set; }
        public DocumentRejected(IApplicationDocument applicationDocument)
        {
            ApplicationDocument = applicationDocument;
        }
    }
}
