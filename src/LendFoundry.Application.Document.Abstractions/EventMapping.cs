﻿namespace CLendFoundry.Application.Document
{
    public class EventMapping
    {
        public string Name { get; set; }
        public string ApplicationNumber { get; set; }
    }
}
