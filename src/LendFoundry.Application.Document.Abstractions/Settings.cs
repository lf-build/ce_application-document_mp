﻿using System;

namespace LendFoundry.Application.Document
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "application-documents";
    }
}
