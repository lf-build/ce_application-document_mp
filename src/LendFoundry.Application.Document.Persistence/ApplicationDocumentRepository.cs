﻿
using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Application.Document.Persistence
{
    public class ApplicationDocumentRepository : MongoRepository<IApplicationDocument, ApplicationDocument>, IApplicationDocumentRepository
    {

        static ApplicationDocumentRepository()
        {           
            BsonClassMap.RegisterClassMap<ApplicationDocument>(map =>
            {
                map.AutoMap();
                var type = typeof(ApplicationDocument);
                map.MapProperty(p => p.DocumentStatus).SetSerializer(new EnumSerializer<DocumentVerificationStatus>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });          
        }
        public ApplicationDocumentRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "application-document")
        {
            CreateIndexIfNotExists
            (
               indexName: "entity-type-id",
               index: Builders<IApplicationDocument>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.ApplicationId)
            );

            CreateIndexIfNotExists
            (
               indexName: "applicationId",
               index: Builders<IApplicationDocument>.IndexKeys.Ascending(i => i.ApplicationId)
            );

            CreateIndexIfNotExists
            (
               indexName: "entity-type-id-catagory",
               index: Builders<IApplicationDocument>.IndexKeys.Ascending(i => i.EntityType).Ascending(i => i.ApplicationId).Ascending(i=>i.Category)
            );

            CreateIndexIfNotExists
            (
               indexName: "tenant-entity-type-id-catagory",
               index: Builders<IApplicationDocument>.IndexKeys.Ascending(i=>i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.ApplicationId).Ascending(i => i.Category)
            );
        }

      
        public async Task<List<IApplicationDocument>> GetByApplicantId(string applicantId)
        {
            return await Query.Where(a => a.ApplicantId == applicantId).ToListAsync();

        }

        public async Task<List<IApplicationDocument>> GetByApplicationId(string entityType , string applicationId)
        {
            return await Query.Where(a =>a.EntityType == entityType && a.ApplicationId == applicationId).ToListAsync();
         
        }

        public async Task<IApplicationDocument> GetByDocumentId(string documentId)
        {
            return await Query.FirstOrDefaultAsync(a => a.DocumentId == documentId);

        }

        public async Task<List<IApplicationDocument>> GetByCategory(string entityType, string applicationId, string category)
        {
            return await Query.Where(a => a.EntityType == entityType && a.ApplicationId == applicationId && a.Category == category).ToListAsync();
          
        }
        public async Task<List<IApplicationDocument>> GetByCategory(string entityType, string applicationId, string[] category)
        {
            return await Query.Where(a => a.EntityType == entityType && a.ApplicationId == applicationId && category.Contains(a.Category)).ToListAsync();

        }





    }
}
