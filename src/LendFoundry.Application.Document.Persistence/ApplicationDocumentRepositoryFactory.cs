﻿
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace LendFoundry.Application.Document.Persistence
{
    public class ApplicationDocumentRepositoryFactory: IApplicationDocumentRepositoryFactory
    {
        public ApplicationDocumentRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IApplicationDocumentRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfigurationFactory = Provider.GetService<IMongoConfigurationFactory>();
            var configuration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            return new ApplicationDocumentRepository(tenantService, configuration);
        }
    }
}
