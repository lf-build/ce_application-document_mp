﻿using LendFoundry.Application.Document.Persistence;
using LendFoundry.Configuration.Client;
using LendFoundry.DocumentManager.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using System.Collections.Generic;

#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif
using System;
using System.Runtime;
using LendFoundry.Configuration;

namespace LendFoundry.Application.Document.Api
{
    internal class Startup
    {
        public Startup(IHostingEnvironment env) { }
        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "ApplicationDocument"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme() 
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> 
                { 
					{ "Bearer", new string[]{} }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.Application.Document.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddDocumentManager();
            services.AddTenantService();
            services.AddLookupService();
            services.AddEventHub(Settings.ServiceName);

            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);
            services.AddMongoConfiguration(Settings.ServiceName);
            services.AddTransient(p => p.GetRequiredService<IConfigurationService<Configuration>>().Get());

            services.AddTransient<IApplicationDocumentRepository, ApplicationDocumentRepository>();
            services.AddTransient<IApplicationDocumentRepositoryFactory, ApplicationDocumentRepositoryFactory>();
            services.AddTransient<IApplicationDocumentService, ApplicationDocumentService>();

            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
            Console.WriteLine(GCSettings.LatencyMode);
            Console.WriteLine(GCSettings.IsServerGC);
            Console.WriteLine(GCSettings.LargeObjectHeapCompactionMode);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);

            // Enable middleware to serve generated Swagger as a JSON endpoint.
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "ApplicationDocument Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseConfigurationCacheDependency();
        }
    }
}
