﻿using LendFoundry.Foundation.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using RestSharp.Extensions;
using System;
using System.Net;
using LendFoundry.Application.Document.Api.ActionResults;
using LendFoundry.DocumentManager;
using System.Collections;

#if DOTNET2
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
#else
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Net.Http.Headers;
#endif

namespace LendFoundry.Application.Document.Api.Controllers
{
    /// <summary>
    /// Controller
    /// </summary>
    /// <seealso cref="LendFoundry.Foundation.Services.ExtendedController" />
    [Route("/")]
    public class ApplicationDocumentController : ExtendedController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationDocumentController"/> class.
        /// </summary>
        /// <param name="applicationDocument">The application document.</param>
        public ApplicationDocumentController(IApplicationDocumentService applicationDocument)
        {
            ApplicationDocumentService = applicationDocument;
        }

        private IApplicationDocumentService ApplicationDocumentService { get; }

        /// <summary>
        /// Creates the document.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="category">The category.</param>
        /// <param name="file">The file.</param>
        /// <param name="metaData">The meta data.</param>
        /// <param name="tags">The tags.</param>
        /// <returns>application document</returns>
        [HttpPost("{applicationId}/{category}/{*tags}")]
        [ProducesResponseType(typeof(IApplicationDocument), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> CreateDocument(string applicationId, string category, IFormFile file, string metaData, string tags)
        {
            if (file == null)
                return ErrorResult.BadRequest("Please upload file.");

            var fileName = ContentDispositionHeaderValue
                .Parse(file.ContentDisposition)
                .FileName
                .Trim('"');

            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationDocumentService.Add(applicationId, category,
                            file.OpenReadStream().ReadAsBytes(), fileName, TagValidation(tags), metaData)));
        }

        /// <summary>
        /// Creates the document.
        /// </summary>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="applicantId">The applicant identifier.</param>
        /// <param name="category">The category.</param>
        /// <param name="file">The file.</param>
        /// <param name="tags">The tags.</param>
        /// <returns> Application Document </returns>
        [HttpPost("{applicationId}/applicant/{applicantId}/{category}/{*tags}")]
        [ProducesResponseType(typeof(IApplicationDocument), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> CreateDocument(string applicationId, string applicantId, string category, IFormFile file, string tags)
        {
            if (file == null)
                return ErrorResult.BadRequest("Please upload file.");

            var fileName = ContentDispositionHeaderValue
                         .Parse(file.ContentDisposition)
                         .FileName
                         .Trim('"');

            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationDocumentService.Add(applicationId, applicantId, category,
                            file.OpenReadStream().ReadAsBytes(), fileName, TagValidation(tags))));
        }

        /// <summary>
        /// Creates the document.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="category">The category.</param>
        /// <param name="file">The file.</param>
        /// <param name="metaData">The meta data.</param>
        /// <param name="tags">The tags.</param>
        /// <returns>Application Document </returns>
        [HttpPost("{entityType}/{applicationId}/{category}/{*tags}")]
        [ProducesResponseType(typeof(IApplicationDocument), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> CreateDocument(string entityType, string applicationId, string category, IFormFile file, string metaData, string tags)
        {
            if (file == null)
                return ErrorResult.BadRequest("Please upload file.");

            var fileName = ContentDispositionHeaderValue
                .Parse(file.ContentDisposition)
                .FileName
                .Trim('"');

            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationDocumentService.Add(entityType, applicationId, category,
                            file.OpenReadStream().ReadAsBytes(), fileName, TagValidation(tags), metaData)));
        }


        /// <summary>
        /// Creates the document.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="applicationId">The application identifier.</param>
        /// <param name="applicantId">The applicant identifier.</param>
        /// <param name="category">The category.</param>
        /// <param name="file">The file.</param>
        /// <param name="tags">The tags.</param>
        /// <returns>Application Document </returns>
        [HttpPost("{entityType}/{applicationId}/applicant/{applicantId}/{category}/{*tags}")]
        [ProducesResponseType(typeof(IApplicationDocument), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> CreateDocument(string entityType, string applicationId, string applicantId, string category, IFormFile file, string tags)
        {
            if (file == null)
                return ErrorResult.BadRequest("Please upload file.");

            var fileName = ContentDispositionHeaderValue
                         .Parse(file.ContentDisposition)
                         .FileName
                         .Trim('"');

            return
                await ExecuteAsync(
                    async () =>
                        Ok(await ApplicationDocumentService.Add(entityType, applicationId, applicantId, category,
                            file.OpenReadStream().ReadAsBytes(), fileName, TagValidation(tags))));
        }

        /// <summary>
        /// Gets the by application number.
        /// </summary>
        /// <param name="applicationNumber">The application number.</param>
        /// <returns>Application Document </returns>
        [HttpGet("{applicationNumber}")]
        [ProducesResponseType(typeof(IEnumerable<IDocumentWithApplication>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetByApplicationNumber(string applicationNumber)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationDocumentService.GetByApplicationNumber(applicationNumber)));
        }

        /// <summary>
        /// Gets the by application number.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="applicationNumber">The application number.</param>
        /// <returns>Application Document </returns>
        [HttpGet("by/entity/{entityType}/{applicationNumber}")]
        [ProducesResponseType(typeof(IEnumerable<IDocumentWithApplication>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetByApplicationNumber(string entityType, string applicationNumber)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationDocumentService.GetByApplicationNumber(entityType, applicationNumber)));
        }


        /// <summary>
        /// Gets the by category.
        /// </summary>
        /// <param name="applicationNumber">The application number.</param>
        /// <param name="category">The category.</param>
        /// <returns>Document </returns>
        [HttpGet("{applicationNumber}/{category}")]
        [ProducesResponseType(typeof(List<IDocument>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetByCategory(string applicationNumber, string category)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationDocumentService.GetByCategory(applicationNumber, category)));

        }

        /// <summary>
        /// Gets the by category.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="applicationNumber">The application number.</param>
        /// <param name="category">The category.</param>
        /// <returns>Document</returns>
        [HttpGet("{entityType}/{applicationNumber}/{category}")]
        [ProducesResponseType(typeof(List<IDocument>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetByCategory(string entityType, string applicationNumber, string category)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationDocumentService.GetByCategory(entityType, applicationNumber, category)));

        }


        /// <summary>
        /// Changes the category.
        /// </summary>
        /// <param name="applicationNumber">The application number.</param>
        /// <param name="documentId">The document identifier.</param>
        /// <param name="category">The category.</param>
        /// <returns>Application Document</returns>
        [HttpPut("{applicationNumber}/change/{documentId}/{category}")]
        [ProducesResponseType(typeof(IApplicationDocument), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> ChangeCategory(string applicationNumber, string documentId, string category)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationDocumentService.ChangeCategory(applicationNumber, documentId, category)));

        }

        /// <summary>
        /// Changes the category.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="applicationNumber">The application number.</param>
        /// <param name="documentId">The document identifier.</param>
        /// <param name="category">The category.</param>
        /// <returns>Application Document</returns>
        [HttpPut("{entityType}/{applicationNumber}/change/{documentId}/{category}")]
        [ProducesResponseType(typeof(IApplicationDocument), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> ChangeCategory(string entityType, string applicationNumber, string documentId, string category)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationDocumentService.ChangeCategory(entityType, applicationNumber, documentId, category)));

        }

        /// <summary>
        /// Verifies the document.
        /// </summary>
        /// <param name="applicationNumber">The application number.</param>
        /// <param name="documentId">The document identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns>Application Document</returns>
        [HttpPut("{applicationNumber}/{documentId}/{status}")]
        [ProducesResponseType(typeof(IApplicationDocument), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> VerifyDocument(string applicationNumber, string documentId, string status)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationDocumentService.VerifyDocument(applicationNumber, documentId, status)));

        }

        /// <summary>
        /// Verifies the document.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="applicationNumber">The application number.</param>
        /// <param name="documentId">The document identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns>Application Document</returns>
        [HttpPut("{entityType}/{applicationNumber}/{documentId}/{status}")]
        [ProducesResponseType(typeof(IApplicationDocument), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> VerifyDocument(string entityType, string applicationNumber, string documentId, string status)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationDocumentService.VerifyDocument(entityType, applicationNumber, documentId, status)));

        }

        /// <summary>
        /// Downloads the documents by application number.
        /// </summary>
        /// <param name="applicationNumber">The application number.</param>
        /// <returns>Downloads Document</returns>
        [HttpGet("{applicationNumber}/download")]
        [ProducesResponseType(204)]
        public async Task<IActionResult> DownloadDocumentsByApplicationNumber(string applicationNumber)
        {
            try
            {
                var bytes = await ApplicationDocumentService.DownloadDocumentsByApplicationNumber(applicationNumber);
                return new FileActionResult("ApplicationDocuments.zip", bytes);
            }
            catch (ArgumentException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return ErrorResult.NotFound(ex.Message);
            }
            catch (InvalidOperationException)
            {
                return ErrorResult.BadRequest("Cannot generate zip file for the documents");
            }
        }


        /// <summary>
        /// Downloads the documents by application number.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="applicationNumber">The application number.</param>
        /// <returns>Downloads Document</returns>
        [HttpGet("{entityType}/{applicationNumber}/download")]
        [ProducesResponseType(204)]
        public async Task<IActionResult> DownloadDocumentsByApplicationNumber(string entityType, string applicationNumber)
        {
            try
            {
                var bytes = await ApplicationDocumentService.DownloadDocumentsByApplicationNumber(entityType, applicationNumber);
                return new FileActionResult("ApplicationDocuments.zip", bytes);
            }
            catch (ArgumentException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return ErrorResult.NotFound(ex.Message);
            }
            catch (InvalidOperationException)
            {
                return ErrorResult.BadRequest("Cannot generate zip file for the documents");
            }
        }

        /// <summary>
        /// Downloads the documents by category.
        /// </summary>
        /// <param name="applicationNumber">The application number.</param>
        /// <param name="category">The category.</param>
        /// <returns>Downloads Document</returns>
        [HttpPost("{applicationNumber}/download")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> DownloadDocumentsByCategory(string applicationNumber, [FromBody]object category)
        {
            try
            {
                if (category != null)
                {
                    var categorylist = ((IEnumerable)category).Cast<object>()
                                     .Select(x => x.ToString())
                                     .ToArray();
                    var bytes = await ApplicationDocumentService.DownloadDocumentsByCategory(applicationNumber, categorylist);
                    return new FileActionResult("ApplicationDocuments.zip", bytes);
                }
                else
                {
                    return ErrorResult.BadRequest("Please provide application document category.");
                }
            }
            catch (ArgumentException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return ErrorResult.NotFound(ex.Message);
            }
            catch (InvalidOperationException)
            {
                return ErrorResult.BadRequest("Cannot generate zip file for the documents");
            }
        }

        /// <summary>
        /// Downloads the documents by category.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="applicationNumber">The application number.</param>
        /// <param name="category">The category.</param>
        /// <returns>Downloads Document</returns>
        [HttpPost("{entityType}/{applicationNumber}/download")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> DownloadDocumentsByCategory(string entityType, string applicationNumber, [FromBody]object category)
        {
            try
            {
                if (category != null)
                {
                    var categorylist = ((IEnumerable)category).Cast<object>()
                                     .Select(x => x.ToString())
                                     .ToArray();
                    var bytes = await ApplicationDocumentService.DownloadDocumentsByCategory(entityType, applicationNumber, categorylist);
                    return new FileActionResult("ApplicationDocuments.zip", bytes);
                }
                else
                {
                    return ErrorResult.BadRequest("Please provide application document category.");
                }
            }
            catch (ArgumentException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return ErrorResult.NotFound(ex.Message);
            }
            catch (InvalidOperationException)
            {
                return ErrorResult.BadRequest("Cannot generate zip file for the documents");
            }
        }

        /// <summary>
        /// Downloads the document base64 format.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="documentId">The document identifier.</param>
        /// <returns>Downloads Document</returns>
        [HttpGet("{entityType}/{entityId}/{documentId}/downloaddocument")]
        [ProducesResponseType(typeof(IDownloadResponseModel), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> DownloadDocumentBase64Format(string entityType, string entityId, string documentId)
        {

            return Ok(await ApplicationDocumentService.DownloadDocumentByDocumentId(entityType, entityId, documentId));

        }

        /// <summary>
        /// Downloads the verified documents.
        /// </summary>
        /// <param name="applicationNumber">The application number.</param>
        /// <param name="category">The category.</param>
        /// <returns>Downloads Document</returns>
        [HttpPost("{applicationNumber}/verifiable/download")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> DownloadVerifiedDocuments(string applicationNumber, [FromBody]object category)
        {
            try
            {
                if (category != null)
                {
                    var categorylist = ((IEnumerable)category).Cast<object>()
                                     .Select(x => x.ToString())
                                     .ToArray();
                    var bytes = await ApplicationDocumentService.DownloadVerifiedDocuments(applicationNumber, categorylist);
                    return new FileActionResult("ApplicationDocuments.zip", bytes);
                }
                else
                {
                    return ErrorResult.BadRequest("Please provide application document category.");
                }
            }
            catch (ArgumentException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return ErrorResult.NotFound(ex.Message);
            }
            catch (InvalidOperationException)
            {
                return ErrorResult.BadRequest("Cannot generate zip file for the documents");
            }
        }

        /// <summary>
        /// Downloads the verified documents.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="applicationNumber">The application number.</param>
        /// <param name="category">The category.</param>
        /// <returns>Downloads Document</returns>
        [HttpPost("{entityType}/{applicationNumber}/verifiable/download")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> DownloadVerifiedDocuments(string entityType, string applicationNumber, [FromBody]object category)
        {
            try
            {
                if (category != null)
                {
                    var categorylist = ((IEnumerable)category).Cast<object>()
                                     .Select(x => x.ToString())
                                     .ToArray();
                    var bytes = await ApplicationDocumentService.DownloadVerifiedDocuments(entityType, applicationNumber, categorylist);
                    return new FileActionResult("ApplicationDocuments.zip", bytes);
                }
                else
                {
                    return ErrorResult.BadRequest("Please provide application document category.");
                }
            }
            catch (ArgumentException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return ErrorResult.NotFound(ex.Message);
            }
            catch (InvalidOperationException)
            {
                return ErrorResult.BadRequest("Cannot generate zip file for the documents");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="applicationNumber"></param>
        /// <param name="documentId"></param>
        /// <param name="fcustatus"></param>
        /// <returns></returns>
        [HttpPut("{entityType}/{applicationNumber}/updatefcustatus/{documentId}/{fcustatus}")]
        [ProducesResponseType(typeof(IApplicationDocument), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> UpdateDocumentFcuStatus(string entityType, string applicationNumber, string documentId, string fcustatus)
        {
            return await ExecuteAsync(async () => Ok(await ApplicationDocumentService.UpdateDocumentFcuStatus(entityType, applicationNumber, documentId, fcustatus)));
        }
        private static IEnumerable<string> SplitTags(string tags)
        {
            return string.IsNullOrWhiteSpace(tags)
                ? new List<string>()
                : tags.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        private static List<string> TagValidation(string tags)
        {
            if (string.IsNullOrWhiteSpace(tags))
                return null;

            tags = WebUtility.UrlDecode(tags);
            var tagList = SplitTags(tags);
            return tagList.ToList();
        }
    }
}
