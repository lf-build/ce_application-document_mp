﻿using LendFoundry.Application.Document.Configurations;
using LendFoundry.DocumentManager;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Application.Document
{
    public class ApplicationDocumentService : IApplicationDocumentService
    {
        #region Constructor

        public ApplicationDocumentService(IApplicationDocumentRepository applicationDocumentRepository, IDocumentManagerService documentManagerService, ILookupService lookupService, Configuration applicationDocumentconfiguration, IEventHubClient eventHubClient)
        {
            ApplicationDocumentRepository = applicationDocumentRepository;
            DocumentManager = documentManagerService;
            LookupService = lookupService;
            ApplicationDocumentConfiguration = applicationDocumentconfiguration;
            EventHubClient = eventHubClient;
        }

        #endregion

        #region Private Variables

        private IEventHubClient EventHubClient { get; }
        private ILookupService LookupService { get; }
        private IApplicationDocumentRepository ApplicationDocumentRepository { get; }
        private IDocumentManagerService DocumentManager { get; }
        private Configuration ApplicationDocumentConfiguration { get; set; }

        #endregion

        private async Task<IApplicationDocument> AddApplication(string entityType ,string applicationNumber, string applicantId, string category, byte[] fileBytes, string fileName, List<string> tags, object metaData=null)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException($"{nameof(entityType)} cannot be empty");

            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentNullException($"{nameof(applicationNumber)} cannot be empty");

            if (fileBytes == null)
                throw new ArgumentNullException(nameof(fileBytes));
            category = category.ToLower();
            var categoryConfiguration = EnsureCategory(entityType, category);

            string modifiedFilename = fileName;
            if (ApplicationDocumentConfiguration.Entities.FirstOrDefault().IsPreFixApplicationNumber)
                modifiedFilename = applicationNumber + "-" + modifiedFilename;

            var uploadedDocument = await DocumentManager.Create(new MemoryStream(fileBytes), entityType, applicationNumber, modifiedFilename, metaData, tags);

            if (uploadedDocument == null)
                throw new DocumentException("The Document cannot be uploaded");

            if (string.IsNullOrWhiteSpace(uploadedDocument.Id))
                throw new DocumentException($"{nameof(uploadedDocument.FileName)} is not found");

            var applicationdocument = new ApplicationDocument
            {
                EntityType = entityType,
                ApplicationId = applicationNumber,
                ApplicantId = applicantId,
                Category = category,
                DocumentId = uploadedDocument.Id,
                GroupName = categoryConfiguration.GroupName,
                DocumentStatus = DocumentVerificationStatus.Verifiable,
                Timestamp= uploadedDocument.Timestamp,
                DocumentFcuStatus = ""
            };

            ApplicationDocumentRepository.Add(applicationdocument);
            return applicationdocument;
        }

        private string EnsureCategoryIsValid(string category)
        {
            if (string.IsNullOrWhiteSpace(category))
                throw new ArgumentNullException($"{nameof(category)} cannot be empty");

            category = category.ToLower();

            var categoryExists = LookupService.GetLookupEntry("applicationDocument-category", category);
            if (categoryExists == null)
                throw new InvalidArgumentException("The category  doesn't exist in the list of categories.");

            return category;
        }

        public async Task<IApplicationDocument> Add(string applicationNumber, string category, byte[] fileBytes, string fileName, List<string> tags, object metaData = null)
        {
            return await AddApplication("application",applicationNumber, null, category, fileBytes, fileName, tags, metaData);
        }

        public async Task<IApplicationDocument> Add(string applicationNumber, string applicantId, string category, byte[] fileBytes, string fileName, List<string> tags)
        {
            return await AddApplication("application", applicationNumber, applicantId, category, fileBytes, fileName, tags);
        }
        
        public async Task<IApplicationDocument> Add(string entityType, string applicationNumber, string category, byte[] fileBytes, string fileName, List<string> tags, object metaData = null)
        {
            return await AddApplication(entityType, applicationNumber, null, category, fileBytes, fileName, tags, metaData);
        }
        
        public async Task<IApplicationDocument> Add(string entityType, string applicationNumber, string applicantId, string category, byte[] fileBytes, string fileName, List<string> tags)
        {
            return await AddApplication(entityType , applicationNumber, applicantId, category, fileBytes, fileName, tags);
        }

        public async Task<IEnumerable<IDocumentWithApplication>> GetByApplicationNumber(string applicationNumber)
        {
            return await GetDocument("application", applicationNumber);
        }
        
        public async Task<IEnumerable<IDocumentWithApplication>> GetByApplicationNumber(string entityType, string applicationNumber)
        {
            return await GetDocument(entityType, applicationNumber);
        }

        private async Task<IEnumerable<IDocumentWithApplication>> GetDocument(string entityType, string applicationNumber)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException($"{nameof(entityType)} cannot be empty");

            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicationNumber));

            var documents = await DocumentManager.GetAll(entityType, applicationNumber);
            var applicationDocuments = await ApplicationDocumentRepository.GetByApplicationId(entityType ,applicationNumber);

            //merged list of application categories with documents
            var result = documents.Join(applicationDocuments, 
                                        arg => arg.Id, 
                                        arg => arg.DocumentId,
                                        (first, second) => 
                                        new DocumentWithApplication {
                                            ApplicantId = second.ApplicantId,
                                            ApplicationId = second.ApplicationId,
                                            EntityType = entityType,
                                            Category = second.Category,
                                            FileName = first.FileName,
                                            Id = first.Id, Metadata = first.Metadata,
                                            Size = first.Size,
                                            Tags = first.Tags,
                                            Timestamp = first.Timestamp,
                                            DocumentStatus = second.DocumentStatus,
                                            DocumentFcuStatus = second.DocumentFcuStatus
                                        });

            return result;
        }

        private async Task<IEnumerable<IDocumentWithApplication>> GetDocumentByCategory(string entityType, string applicationNumber, string[] category)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException($"{nameof(entityType)} cannot be empty");

            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicationNumber));

            var documents = await DocumentManager.GetAll("application", applicationNumber);
            var applicationDocuments = await ApplicationDocumentRepository.GetByCategory(entityType,applicationNumber, category);

            //merged list of application categories with documents
            var result = documents.Join(applicationDocuments, arg => arg.Id, arg => arg.DocumentId,
                (first, second) => new DocumentWithApplication { ApplicantId = second.ApplicantId, ApplicationId = second.ApplicationId, EntityType = entityType, Category = second.Category, FileName = first.FileName, Id = first.Id, Metadata = first.Metadata, Size = first.Size, Tags = first.Tags, Timestamp = first.Timestamp, DocumentStatus = second.DocumentStatus, DocumentId = second.DocumentId });

            return result;
        }

        public async Task<IEnumerable<IDocumentWithApplication>> GetByCategory(string entityType, string applicationNumber, string[] category)
        {
            return await GetDocumentByCategory(entityType, applicationNumber, category);
        }

        public async Task<IEnumerable<IDocumentWithApplication>> GetByCategory(string applicationNumber, string[] category)
        {
            return await GetDocumentByCategory("application", applicationNumber, category);
        }

        private async Task<List<IDocument>> GetDocumentByCategory(string entityType, string applicationNumber, string category)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException($"{nameof(entityType)} cannot be empty");

            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicationNumber));

            if (string.IsNullOrWhiteSpace(category))
                throw new ArgumentException("Argument is null or whitespace", nameof(category));

            var documents = await DocumentManager.GetAll("application", applicationNumber);
            if (documents == null)
                throw new NotFoundException($"Application Documents {applicationNumber} not found in document Manager");

            var documentIds = new List<string>();
            var applicationDocuments = await ApplicationDocumentRepository.GetByCategory(entityType,applicationNumber, category);
            if (applicationDocuments != null)
                documentIds.AddRange(applicationDocuments.Select(item => item.DocumentId));

            return documents.Where(a => documentIds.Contains(a.Id)).ToList();
        }

        public async Task<List<IDocument>> GetByCategory(string entityType, string applicationNumber, string category)
        {
            return await GetDocumentByCategory(entityType, applicationNumber, category);
        }

        public async Task<List<IDocument>> GetByCategory(string applicationNumber, string category)
        {
            return await GetDocumentByCategory("application", applicationNumber, category);
        }

        private async Task<byte[]> DownloadDocuments(string entityType, string applicationNumber, string[] category)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException($"{nameof(entityType)} cannot be empty");

            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicationNumber));

            //get all the documents to be downloaded
            var documents = await GetByCategory(entityType,applicationNumber, category);
            string[] documentId = documents.Select(x => x.Id).ToArray();
            if (documents == null || documents?.Count()==0)
                throw new NotFoundException($"Documents {applicationNumber} not found in document Manager");

            return await ZipDocumentsAsync(entityType ,applicationNumber, documentId);
        }

        public async Task<byte[]> DownloadDocumentsByCategory(string entityType, string applicationNumber, string[] category)
        {
            return await DownloadDocuments(entityType, applicationNumber, category);
        }
        
        public async Task<byte[]> DownloadDocumentsByCategory(string applicationNumber, string[] category)
        {
            return await DownloadDocuments("application", applicationNumber, category);
        }

        //TODO: we can move this method to DocumentManager to save some bandwidth
        private async Task<byte[]> DownloadDocuments(string entityType, string applicationNumber)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicationNumber));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Argument is null or whitespace", nameof(entityType));

            //get all the documents to be downloaded
            var documents = await GetByApplicationNumber(entityType,applicationNumber);
            string[] documentId = documents.Select(x => x.Id).ToArray();
            if (documents == null || documents?.Count() == 0)
                throw new NotFoundException($"Application Documents {applicationNumber} not found in document Manager");

            return await ZipDocumentsAsync(entityType,applicationNumber, documentId);
        }
        
        public async Task<byte[]> DownloadDocumentsByApplicationNumber(string entityType, string applicationNumber)
        {
            return await DownloadDocuments(entityType, applicationNumber);
        }
        
        public async Task<byte[]> DownloadDocumentsByApplicationNumber(string applicationNumber)
        {
            return await DownloadDocuments("application", applicationNumber);
        }

        private async Task<byte[]> ZipDocumentsAsync(string entityType, string applicationNumber,string[] documentId)
        {

            using (var outStream = await DocumentManager.DownloadZipDocument(entityType, applicationNumber, documentId))
            {
                using (var reader = new BinaryReader(outStream))
                {
                    return reader.ReadBytes((int)outStream.Length);
                }

            }
        }

        private async Task<IApplicationDocument> ChangeDocumentCategory(string entityType, string applicationNumber, string documentId, string category )
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicationNumber));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Argument is null or whitespace", nameof(entityType));

            if (string.IsNullOrWhiteSpace(category))
                throw new ArgumentException("Argument is null or whitespace", nameof(category));
            var categoryDetails = EnsureCategory(entityType, category);
           
            if (string.IsNullOrWhiteSpace(documentId))
                throw new ArgumentException("Argument is null or whitespace", nameof(documentId));

            var document = await ApplicationDocumentRepository.GetByDocumentId(documentId);
            if (document == null)
                throw new NotFoundException($"Documents {applicationNumber} not found in document Manager");
            var oldCategory = document.Category;
            document.Category = category;
            document.GroupName = categoryDetails.GroupName;

            ApplicationDocumentRepository.Update(document);
            await EventHubClient.Publish("ApplicationDocumentCategoryChanged", new { Document = document, OldCategory = oldCategory });
            return document;
        }
        
        public async Task<IApplicationDocument> ChangeCategory(string entityType, string applicationNumber, string documentId, string category)
        {
            return await ChangeDocumentCategory(entityType, applicationNumber, documentId, category);
        }
        
        public async Task<IApplicationDocument> ChangeCategory(string applicationNumber, string documentId, string category)
        {
            return await ChangeDocumentCategory("application", applicationNumber, documentId, category);
        }

        private async Task<IApplicationDocument> Verify(string entityType, string applicationNumber, string documentId,string status)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicationNumber));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Argument is null or whitespace", nameof(entityType));


            if (string.IsNullOrWhiteSpace(documentId))
                throw new ArgumentException("Argument is null or whitespace", nameof(documentId));

            var content = (DocumentVerificationStatus)Enum.Parse(typeof(DocumentVerificationStatus), status);

            var document = await ApplicationDocumentRepository.GetByDocumentId(documentId);
            if (document == null)
                throw new NotFoundException($"{entityType} Documents {applicationNumber} not found in document Manager");
          
            document.DocumentStatus = content;
            ApplicationDocumentRepository.Update(document);

            await EventHubClient.Publish("DocumentVerified", document);
            return document;
        }
        
        public async Task<IApplicationDocument> VerifyDocument(string entityType, string applicationNumber, string documentId, string status)
        {
            return await Verify(entityType, applicationNumber, documentId, status);
        }
        
        public async Task<IApplicationDocument> VerifyDocument(string applicationNumber, string documentId, string status)
        {
            return await Verify("application", applicationNumber, documentId, status);
        }

        public async Task<IDownloadResponseModel> DownloadDocumentByDocumentId(string entityType, string entityId, string documentId)
        {

            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException($"{nameof(entityType)} cannot be empty");

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException($"{nameof(entityId)} cannot be empty");


            var DocumentAvailable = await DocumentManager.Get(entityType, entityId, documentId);

            if (DocumentAvailable == null)
                throw new ArgumentNullException($"{nameof(documentId)} cannot be empty", "Document is not found");

            var download = await DocumentManager.Download(entityType, entityId, documentId);
            if (download == null)
                throw new ArgumentNullException(nameof(download));

            IDownloadResponseModel downloadresponse = new DownloadResponseModel();
            var byteArray = ReadFully(download);
            downloadresponse.DownloadString = Convert.ToBase64String(byteArray);
            return downloadresponse;
        }

        private async Task<byte[]> DownloadVerifiedDocument(string entityType, string applicationNumber, string[] category)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicationNumber));

            //get all the documents to be downloaded
            var documents = await GetByCategory(applicationNumber, category);
            var verifiedDocuments = documents.Where(x => x.DocumentStatus.Equals(DocumentVerificationStatus.Verifiable));
            string[] documentId = verifiedDocuments.Select(x => x.Id).ToArray();
            if (verifiedDocuments == null || verifiedDocuments?.Count() == 0)
                throw new NotFoundException($"Application Documents {applicationNumber} not found in document Manager");

            return await ZipDocumentsAsync(entityType,applicationNumber, documentId);
        }

        public async Task<byte[]> DownloadVerifiedDocuments(string entityType, string applicationNumber, string[] category)
        {
            return await DownloadVerifiedDocument(entityType, applicationNumber, category);
        }

        public async Task<byte[]> DownloadVerifiedDocuments(string applicationNumber, string[] category)
        {
            return await DownloadVerifiedDocument("application", applicationNumber, category);
        }

        public async Task<IApplicationDocument> UpdateDocumentFcuStatus(string entityType, string applicationNumber, string documentId, string fcustatus)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentNullException("Application number is mandatory", nameof(applicationNumber));
            
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException("Entity Type is mandatory", nameof(entityType));

            if (string.IsNullOrWhiteSpace(documentId))
                throw new ArgumentNullException("Document Id is mandatory", nameof(documentId));

            if (string.IsNullOrWhiteSpace(fcustatus))
                throw new ArgumentNullException("fcustatus Id is mandatory", nameof(documentId));

            var document = await ApplicationDocumentRepository.GetByDocumentId(documentId);
            if (document == null)
                throw new NotFoundException($"{entityType} Documents {applicationNumber} not found in document Manager");

            document.DocumentFcuStatus = fcustatus;
            ApplicationDocumentRepository.Update(document);

            await EventHubClient.Publish("DocumentFCUStatusUpdated", document);
            return document;
        }

        private static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        private static byte[] ConvertStreamToBytes(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        private ApplicationDocumentConfiguration EnsureCategory(string entityType, string category)
        {
            if (ApplicationDocumentConfiguration == null)
                throw new NotFoundException($"The entity:{entityType} was not found in configuration");
            
            var entity = ApplicationDocumentConfiguration.Entities.FirstOrDefault(e => e.EntityName.Equals(entityType, StringComparison.OrdinalIgnoreCase));

            if (entity == null)
                throw new NotFoundException($"The entity:{entityType} was not found in configuration");

            var categoryExists = entity.ApplicationDocument.Where(i => i.Category.Contains(category)).FirstOrDefault();
            if (categoryExists == null)
                throw new InvalidArgumentException("The category  doesn't exist in the list of categories.");

            return categoryExists;
        }
    }
}
