﻿
using LendFoundry.Security.Tokens;

namespace LendFoundry.Application.Document.Client
{
    public interface IApplicationDocumentServiceClientFactory
    {
        IApplicationDocumentService Create(ITokenReader reader);
    }
}
