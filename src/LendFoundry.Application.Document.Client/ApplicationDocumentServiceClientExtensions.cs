﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Application.Document.Client
{
    public static class ApplicationDocumentServiceClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddApplicantDocumentService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IApplicationDocumentServiceClientFactory>(p => new ApplicationDocumentServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IApplicationDocumentServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddApplicantDocumentService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IApplicationDocumentServiceClientFactory>(p => new ApplicationDocumentServiceClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IApplicationDocumentServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddApplicantDocumentService(this IServiceCollection services)
        {
            services.AddTransient<IApplicationDocumentServiceClientFactory>(p => new ApplicationDocumentServiceClientFactory(p));
            services.AddTransient(p => p.GetService<IApplicationDocumentServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
