﻿using System.Collections.Generic;

using LendFoundry.Foundation.Client;
using RestSharp;
using System.Threading.Tasks;
using System.Linq;
using LendFoundry.DocumentManager;
using Newtonsoft.Json;
using System;

namespace LendFoundry.Application.Document.Client
{
    public class ApplicantDocumentService : IApplicationDocumentService
    {
        public ApplicantDocumentService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IApplicationDocument> Add(string applicationId, string applicantId, string category, byte[] request, string fileName, List<string> tags)
        {
            var objRequest = new RestRequest("{applicationId}/applicant/{applicantId}/{category}", Method.POST);
            objRequest.AddUrlSegment("applicationId", applicationId);
            objRequest.AddUrlSegment("applicantId", applicantId);
            objRequest.AddUrlSegment("category", category);
            AppendTags(objRequest, tags);
            objRequest.AddFile("file", request, fileName, "application/octet-stream");
            return await Client.ExecuteAsync<ApplicationDocument>(objRequest);
        }

        public async Task<IApplicationDocument> Add(string applicationId, string category, byte[] request, string fileName, List<string> tags, object metaData = null)
        {
            var objRequest = new RestRequest("{applicationId}/{category}", Method.POST);
            objRequest.AddUrlSegment("applicationId", applicationId);
            objRequest.AddUrlSegment("category", category);            
            if (metaData != null)
            {
                var json = JsonConvert.SerializeObject(metaData);
                objRequest.AddParameter("metaData", json);
            }
                
            objRequest.AddFile("file", request, fileName, "application/octet-stream");
            AppendTags(objRequest, tags);
            return await Client.ExecuteAsync<ApplicationDocument>(objRequest);
        }



        public async Task<IEnumerable<IDocumentWithApplication>> GetByApplicationNumber(string applicationNumber)
        {
            var objRequest = new RestRequest("{applicationNumber}", Method.GET);
            objRequest.AddUrlSegment("applicationNumber", applicationNumber);
            return new List<DocumentWithApplication>(await Client.ExecuteAsync<List<DocumentWithApplication>>(objRequest));
        }

        public async Task<List<IDocument>> GetByCategory(string applicationNumber, string category)
        {
            var objRequest = new RestRequest("{applicationNumber}/{category}", Method.GET);
            objRequest.AddUrlSegment("applicationNumber", applicationNumber);
            objRequest.AddUrlSegment("category", category);
            return new List<IDocument>(await Client.ExecuteAsync<List<LendFoundry.DocumentManager.Document>>(objRequest));
        }

        public async Task<byte[]> DownloadDocumentsByApplicationNumber(string applicationNumber)
        {
            var objRequest = new RestRequest("{applicationNumber}", Method.GET);
            objRequest.AddUrlSegment("applicationNumber", applicationNumber);
            return (await Client.ExecuteAsync<byte[]>(objRequest));
        }

        public async Task<IApplicationDocument> ChangeCategory(string applicationNumber, string documentId , string category)
        {
            var objRequest = new RestRequest("{applicationNumber}/change/{category}/{documentId}", Method.PUT);
            objRequest.AddUrlSegment("applicationNumber", applicationNumber);           
            objRequest.AddUrlSegment("documentId", documentId);
            objRequest.AddUrlSegment("category", category);
            return await Client.ExecuteAsync<ApplicationDocument>(objRequest);
        }

        public async Task<IApplicationDocument> VerifyDocument(string applicationNumber, string documentId, string status)
        {
            var objRequest = new RestRequest("{applicationNumber}/{documentId}/{status}", Method.PUT);
            objRequest.AddUrlSegment("applicationNumber", applicationNumber);
            objRequest.AddUrlSegment("documentId", documentId);
            objRequest.AddUrlSegment("status", status);
            return await Client.ExecuteAsync<ApplicationDocument>(objRequest);
        }
        public async Task<IDownloadResponseModel> DownloadDocumentByDocumentId(string entityType, string entityId, string documentId)
        {
            var objRequest = new RestRequest("{entityType}/{entityId}/{documentId}/downloaddocument", Method.GET);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("entityId", entityId);
            objRequest.AddUrlSegment("documentId", documentId);
            return (await Client.ExecuteAsync<DownloadResponseModel>(objRequest));
        }

        private static void AppendTags(IRestRequest request, IReadOnlyList<string> tags)
        {
            if (tags == null || !tags.Any())
                return;

            var url = request.Resource;
            for (var index = 0; index < tags.Count; index++)
            {
                var tag = tags[index];
                url = url + $"/{{tag{index}}}";
                request.AddUrlSegment($"tag{index}", tag);
            }
            request.Resource = url;
        }

        public async Task<byte[]> DownloadDocumentsByCategory(string applicationNumber, string[] category)
        {
            
            var objRequest = new RestRequest("{applicationNumber}/download", Method.POST);
            objRequest.AddUrlSegment("applicationNumber", applicationNumber);
            objRequest.AddJsonBody(category);
            return (await Client.ExecuteAsync<byte[]>(objRequest));
        }

        public async Task<byte[]> DownloadVerifiedDocuments(string applicationNumber, string[] category)
        {
            var objRequest = new RestRequest("{applicationNumber}/verifiable/download", Method.POST);
            objRequest.AddUrlSegment("applicationNumber", applicationNumber);
            objRequest.AddJsonBody(category);
            return (await Client.ExecuteAsync<byte[]>(objRequest));
        }

        public async Task<IApplicationDocument> Add(string entityType, string applicationNumber, string category, byte[] fileBytes, string fileName, List<string> tags, object metaData = null)
        {
            var objRequest = new RestRequest("{entityType}/{applicationId}/{category}", Method.POST);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("applicationId", applicationNumber);
            objRequest.AddUrlSegment("category", category);
            if (metaData != null)
            {
                var json = JsonConvert.SerializeObject(metaData);
                objRequest.AddParameter("metaData", json);
            }
            objRequest.AddFile("file", fileBytes, fileName, "application/octet-stream");
            AppendTags(objRequest, tags);
            return await Client.ExecuteAsync<ApplicationDocument>(objRequest);
        }

        public async Task<IApplicationDocument> Add(string entityType, string applicationNumber, string applicantId, string category, byte[] fileBytes, string fileName, List<string> tags)
        {
            var objRequest = new RestRequest("{entityType}/{applicationId}/applicant/{applicantId}/{category}", Method.POST);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("applicationId", applicationNumber);
            objRequest.AddUrlSegment("applicantId", applicantId);
            objRequest.AddUrlSegment("category", category);
            AppendTags(objRequest, tags);
            objRequest.AddFile("file", fileBytes, fileName, "application/octet-stream");
            return await Client.ExecuteAsync<ApplicationDocument>(objRequest);
        }

        public async Task<IEnumerable<IDocumentWithApplication>> GetByApplicationNumber(string entityType, string applicationNumber)
        {
            var objRequest = new RestRequest("by/entity/{entityType}/{applicationNumber}", Method.GET);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("applicationNumber", applicationNumber);
            return new List<DocumentWithApplication>(await Client.ExecuteAsync<List<DocumentWithApplication>>(objRequest));
        }

        public async Task<List<IDocument>> GetByCategory(string entityType, string applicationNumber, string category)
        {
            var objRequest = new RestRequest("{entityType}/{applicationNumber}/{category}", Method.GET);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("applicationNumber", applicationNumber);
            objRequest.AddUrlSegment("category", category);
            return new List<IDocument>(await Client.ExecuteAsync<List<LendFoundry.DocumentManager.Document>>(objRequest));
        }

        public async Task<byte[]> DownloadDocumentsByApplicationNumber(string entityType, string applicationNumber)
        {
           
           var objRequest = new RestRequest("{entityType}/{applicationNumber}/download", Method.GET);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("applicationNumber", applicationNumber);
            return (await Client.ExecuteAsync<byte[]>(objRequest));
        }

        public async Task<IApplicationDocument> ChangeCategory(string entityType, string applicationNumber, string documentId, string category)
        {
            var objRequest = new RestRequest("{entityType}/{applicationNumber}/change/{documentId}/{category}", Method.PUT);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("applicationNumber", applicationNumber);
            objRequest.AddUrlSegment("documentId", documentId);
            objRequest.AddUrlSegment("category", category);
            return await Client.ExecuteAsync<ApplicationDocument>(objRequest);
        }

        public async Task<IApplicationDocument> VerifyDocument(string entityType, string applicationNumber, string documentId, string status)
        {
            var objRequest = new RestRequest("{entityType}/{applicationNumber}/{documentId}/{status}", Method.PUT);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("applicationNumber", applicationNumber);
            objRequest.AddUrlSegment("documentId", documentId);
            objRequest.AddUrlSegment("status", status);
            return await Client.ExecuteAsync<ApplicationDocument>(objRequest);
        }

        public async Task<byte[]> DownloadDocumentsByCategory(string entityType, string applicationNumber, string[] category)
        {
            var objRequest = new RestRequest("{entityType}/{applicationNumber}/download", Method.POST);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("applicationNumber", applicationNumber);
            objRequest.AddJsonBody(category);
            return (await Client.ExecuteAsync<byte[]>(objRequest));
        }

        public async Task<byte[]> DownloadVerifiedDocuments(string entityType, string applicationNumber, string[] category)
        {
            var objRequest = new RestRequest("{entityType}/{applicationNumber}/verifiable/download", Method.POST);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("applicationNumber", applicationNumber);
            objRequest.AddJsonBody(category);
            return (await Client.ExecuteAsync<byte[]>(objRequest));
        }

        public async Task<IApplicationDocument> UpdateDocumentFcuStatus(string entityType, string applicationNumber, string documentId, string fcustatus)
        {
            var objRequest = new RestRequest("{entityType}/{applicationNumber}/updatefcustatus/{documentId}/{fcustatus}", Method.PUT);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("applicationNumber", applicationNumber);
            objRequest.AddUrlSegment("documentId", documentId);
            objRequest.AddUrlSegment("fcustatus", fcustatus);
            return await Client.ExecuteAsync<ApplicationDocument>(objRequest);
        }
    }
}
